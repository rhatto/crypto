Listas criptografadas
=====================

A infra-estrutura de comunicação da maioria dos projetos é precária em
termos de privacidade e segurança. Os servidores quem armazena todos os
arquivos de todas as listas, por exemplo, em geral não possuem seu disco
criptografado. Assim, tudo que já trafegou por ele poderia ser
facilmente acessado.

Em momentos de necessidade deve existir um canal seguro para trocar
mensagens e as pessoas devem estar preparadas para usá-lo. Para ajudar
nisso, existem gerenciadores de listas criptografadas.

Existem alguns softwares que já permitem o uso de criptografia em
pequenas listas de discussão por email, funcionando da seguinte forma: a
lista possui uma chave pública e a chave pública de todos os assinantes.
Quando alguém quiser mandar uma mensagem criptografada para a lista,
basta criptografá-la apenas para a chave pública da lista que a lista
irá descriptografá-la, criptografar e enviar individualmente para cada
assinante usando sua respectiva chave pública.

Os seguintes softwares de lista de discussão suportam criptografia
padrão `OpenPGP <http://www.openpgp.org>`_:

-  `schleuder <http://schleuder2.nadir.org/>`_: atualmente o software
   mais recomendado para listas de discussão pela sua capacidade de
   remoção de muitos metadados das mensagens e também por funcionar como
   um re-enviador (remailer) de mensagens.
-  `firma <https://firma.fluxo.info>`_: é um programa pequeno e
   eficiente gerenciador de listas criptografadas; ele foi desenvolvido
   para ignorar mensagens enviadas à lista que não estejam
   criptografadas e assinadas, além de possuir uma interface
   administrativa por email.

Outros softwares semelhantes, porém não recomendados, podem ser
encontrados na `documentação do Firma <https://firma.fluxo.info>`_.

Outras vantagens
----------------

-  Removem boa parte dos metadados das mensagens.
-  Opcionalmente podem ofuscar o remetente nos metadados dos emails.
   Assim, mesmo que alguém capture as mensagens recebidas no servidor,
   nem sempre (já que isso depende de muitos fatores) conseguirá
   descobrir quem as enviou.

Dicas gerais
------------

Para qualquer um destes softwares, valem as seguintes dicas:

-  NÂO SE ESQUEÇAM: mandem apenas mensagens criptografadas e assinadas,
   senão a lista não aceita a mensagem e vocês correm o risco de mandar
   uma mensagem não-criptografada pela internet.

-  Quando for iniciar uma nova thread de mensagens, escolha assuntos sem
   sentido ou pouco informativos, porque o assunto das mensagens é a
   única coisa que não tem jeito de estar criptografado. Não é apenas
   uma limitação dos atuais softwares de listas criptografadas, mas
   interente aos padrões de email.

-  Pra quem usa Thunderbird: verifique se suas mensagens assinadas e/ou
   criptografadas estão com PGP/MIME: no menu OpenPGP da janela
   principal do Thunderbird, existe a opção "Preferências". Clicando
   nela, aparecerá uma janela com diversas abas. Uma dessas abas contém
   opções de PGP/MIME: vá até essa aba e certifique-se que seu
   Thunderbird está configurado para usar sempre PGP/MIME. O PGP/MIME é
   um esquema de enviar a mensagem criptografada e a assinatura como
   anexos. Fica mais bonito :)

-  Como configurar o Thundebird para que sempre que você escreva uma
   mensagem para esta lista ela seja automaticamente criptografada, sem
   que vocês precise selecionar isso no menu OpenPGP da janela de
   composição. Isso é interessante porque pode acontecer de alguém
   mandar uma mensagem para a lista e esquecer de criptografar. A
   mensagem não vai entrar na lista, mas trafegará pela internet sem
   estar criptografada. Vá em OpenPGP / Edit Per-Recipient Rules / Add.
   Daí na janela que abrir você seleciona os emails afetados por essa
   regra, que no caso seria o email da lista; depois seleciona as chaves
   associadas a essa regra, ou seja, a chave da lista; e por último, na
   parte "Defaults for...", você define qual a opção padrão para
   "Signing", "Encrypting" e "PGP/MIME", sendo as opções "Always",
   "Never" e "Yes, if selected in Message Composition".

-  Para enviar mensagens totalmente válidas a uma lista do schleuder, é
   necessário criptografá-las com a chave pública da lista e assiná-las
   com a chave privada do/a rementente, do contrário a lista reclama ou,
   dependendo da configuração, pode até recusar a mensagem.

-  Lembre-se: criptografia não é maquiagem ou coisa chique, mas sim uma
   ferramenta importantíssima. Pode ser difícil usá-la no começo, mas
   todos deveriam fazer um esforço.

Schleuder
---------

Vantagens do schleuder:

-  É possível configurar a lista para apenas enviar mensagens cifradas.
-  Possui capacidade de remailing (repasse de mensagens), permitindo que
   a lista criptografada proteja o remetente de uma mensagem ao
   re-enviá-la para terceiros.

Firma
-----

Vantagens do firma:

-  Não mantém mensagens descriptografadas armazenadas no servidor em
   nenhum momento.
-  Interface administrativa amigável.

