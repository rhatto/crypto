Bate-papo com Conexão Segura (SSL)
==================================

Conforme diz a seção `Criptografia e Internet <internet>`_, a internet é
uma rede na qual as informações podem ser facilmente interceptadas. Isso
quer dizer que usando um sistema de bate-papo comum é possível que mesmo
em conversas privadas alguém escute o seu diálogo.

Para evitar esse tipo de coisa e preservar sua identidade, é possível
utilizar a tecnologia SSL (Secure Sockets Layer), também conhecida como
conexão segura.

A conexão segura utiliza criptografia para conectar você ao servidor.
Tudo o que é enviado ao servidor e dele até você só poderá ser
interpretado por ambas as partes. Seria algo como esse desenho:

::

     _________________                    _______________
    |  _____________  |    internet      |               |
    | | programa de |_|__________________|  servidor de  |
    | | bate-papo   | |  conexão segura  |  bate-papo    |
    |  -------------  |                  |_______________|
    |_________________|

      seu computador

As mensagens, antes de chegar ao servidor ou do servidor até você,
passam por vários computadores e nesse caminho ainda poderá ser
interceptada, mas só poderá ser interpretada se possuir uma das `chaves
privadas <introducao>`_ -- a do seu computador, no caso das mensagens
enviadas pelo servidor, ou a do servidor, no caso das mensagens enviadas
pelo seu computador.

Se você usa conexão segura e está conversando no bate-papo com alguém
que não usa conexão segura, então em as mensagens entre vocês só estarão
criptografadas na metade do caminho entre você e seu interlocutor:

::

     _________________                    _______________                    _________________
    |  _____________  |    internet      |               |    internet      |  _____________  |
    | | programa de |_|__________________|  servidor de  |__________________|_| programa de | |
    | | bate-papo   | |  conexão segura  |  bate-papo    |   conexão comum  | | bate-papo   | |
    |  -------------  |                  |_______________|                  |  -------------  |
    |_________________|                                                     |_________________|
                                                                               computador do
      seu computador                                                          seu interlocutor

Para que a conversa seja realmente segura é indispensável que todas as
partes envolvidas numa conversa utilizem a conexão segura.

Uma outra vantagem desse tipo de conexão é o mascaramento do IP dos
usuários, que é número que identifica cada computador que está na
internet. Se alguém sabe o seu número IP, é bem possível que ela saiba
identificar qual em qual país está o seu computador e qual é o provedor
de acesso.

Por isso, é interessante que o seu IP não esteja disponível para aqueles
que frequentam o mesmo bate-papo que você, e é isso que a conexão segura
faz: todos os usuários com conexão segura aparentam estar utilizando o
próprio servidor de bate-papo como seu computador pessoal.

Configurando uma conexão segura no bate-papo do CMI

Método simples

A forma mais simples de se conectar por conexão segura no bate-papo do
CMI é através do endereço https://irc.indymedia.org/, mas esta forma não
é totalmente segura, ainda sim sendo melhor que uma conexão comum. A
seguir damos instruções para as formas mais seguras de conexão.

Usuários de GNU/Linux

Há várias formas de fazer uma conexão segura. A mais simples é usar um
cliente de IRC com SSL habilitado. A seguir alguns exemplos de clientes
com tal suporte:

-  Irssi
-  Xchat

Para todos eles, o seguinte comando deve funcionar:

::

    /server -SSL irc.indymedia.org 994

Para o Xchat digite:

::

    /sslserver irc.indymedia.org 994

No presente momento, o suporte ao Xchat está em avançado estágio,
conseguindo habilitação em diferentes distribuições de GNU/Linux e
outros Sistemas Operacionais. O método ideal é selecionar as caixinhas
'Usar SSL' e 'Aceitar certificado inválido' na tela 'Lista de
Servidores'. Além disso na caixa Servidores adicionar
'irc.indymedia.org/994'. Quando está conectando você poderá ver
informações sobre o certificado SSL seguido da habitual informação de
conexão do IRC. Verifique que você aparece como irc@127.0.0STOPSPAM.1,
para ter certeza que está conectado atravéz de conexão segura.

Stunnel
-------

O outro, e talvez mais flexível meio de conectar é usando 'stunnel' em
conjunto com o cliente de IRC da sua escolha. O código fonte, assim como
binários para outros Sistemas Operacionais pode ser obtido em
http://www.stunnel.org/download/. Se você está usando Debian GNU/Linux,
você pode simplesmente digitar 'apt-get install stunnel'.

O primeiro passo é criar o túnel seguro entre seu computador e o
irc.indymedia.org. Voce pode ajustar isso na porta que você quiser. Há
algumas questões para manter em mente aqui:

-  Usuários diferentes do root não podem atar-se para portas abaixo da
   1024
-  Muitos firewalls podem bloquear pacotes em portas abaixo de 1024
-  O usuário que estiver executando stunnel precisa ter permissão de
   escrita numa pasta onde ele deixará um arquivo contendo o número do
   processo (pid) que o stunnel estiver rodando (normalmente /var/run)

Para o propósito deste tutorial nós vamos usar a porta local 6994. Ela é
acessivel para os usuários, e assumindo que você tenha acesso para
escrever o arquivo pid, você pode se sair bem. Se tiver problemas, mas
tem acesso ao root disponível, o que você tem a fazer é rodar stunnel
como root. Neste caso sinta-se livre para escolher qualquer porta que
você quiser.

Assumimos o seguinte comando:

::

    stunnel -c -d 6994 -r irc.indymedia.org:994

Isto avisa ao stunnel para ouvir na porta 6994 do seu computador local,
e encaminhar a conexão para a porta 994 no irc.indymedia.org. Digite

::

    stunnel -h

para uma lista completa de descrições destes sinais.

Você deve ter agora ter um túnel seguro habilitado. Para ter certeza,
olhe o processo rodando no plano de fundo. Você pode também ver algo
similar ao seguinte no seu sistema de log (normalmente /var/log/syslog):

::

    Mar 24 21:13:48 yourhost stunnel[11990]: Using 'irc.indymedia.org.994' as tcpwrapper service name  
    Mar 24 21:13:48 yourhost stunnel[11990]: stunnel 3.22 on i586-pc-linux-gnu PTHREAD+LIBWRAP with 
    <nop>OpenSSL? 0.9.6c 21 dec 2001 Mar 24 21:13:48 yourhost stunnel[11991]: FD_SETSIZE=1024, file  
    ulimit=1024 -> 500 clients allowed

A parte difícil do processo está completa. Agora tudo que você tem que
fazer é conectar do lado local do seu túnel com seu cliente de IRC. Não
importa o cliente que você escolha, isto consiste em conectar-se ao
localhost atravéz da porta que você escolheu (no nosso caso 6994). Para
a maioria dos clientes de IRC o seguinte comando bastará :'/server
localhost 6994'. Você deverá conectar-se normalmente.

Quando você faz essa conexão, o stunnel deve reportar algo como isto em
seu sistema de logs:

::

    Mar 24 21:51:30 yourhost stunnel[12073]: irc.indymedia.org.994 connected from 127.0.0.1:2780.

A partir deste ponto você deve estar conectado ao servidor de IRC, e
pode curtir comunicação segura!

No Windows
----------

Baixe o cliente em http://www.xchat.org / http://silverex.org (tenha
certeza de estar baixando a versão Windows) e instale-o. Use o comando

::

    /sslserver irc.indymedia.org 994

para se conectar através de SSL.

Sobre
-----

Trecho extraído do `Guia de Bate-papo seguro do CMI Brasil <http://docs.indymedia.org/view/Sysadmin/SecureIRCpt>`_.
