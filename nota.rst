Nota sobre Modo Texto e Modo Gráfico
====================================

Existem dois modos de interação entre o usuário e o computador, o modo
texto e o modo gráfico. No modo texto o usuário interage através de
comandos fornecidos via teclado, o mouse quase não é usado e a tela do
computador contém apenas caracteres, sem a possibilidade de visualizar
imagens.

Já o Modo Gráfico possibilita que grande parte da interação do usuário
com o computador seja feita com mouse, via botões e outros objetos.

O Modo Gráfico é muito mais intuitivo e simples de usar, porém no caso
da criptografia os programas em modos gráfico tem muito menos recursos
que o GPG no modo texto. Por isso, é interessante que o usuário tenha
noções tanto de usar o modo texto quanto o modo gráfico. Mas se você
estiver com pressa ou tem preguiça de aprender os maravilhosos comandos
do modo texto (também conhecido como console), leia ao menos as seções
deste manual sobre programas no modo gráfico.
