Criptografia no sistema de arquivos
===================================

Uma vez que o seu chaveiro fica guardado no seu disco rígido ou outro
dispositivo de armazenamento, é possível que ele seja copiado por
terceiros. Além disso, você pode querer que dados no seu disco sejam
criptografados e descriptografados de forma transparente, isto é, sem
que você perceba.

Atualmente existem várias implementações de criptografia em disco
rígido, tanto em plataforma Linux quanto Windows. Futuras versões deste
manual conterão explicações mais detalhadas sobre cada uma delas. Por
enquanto, fique com as seguintes referências:

No Linux
--------

No GNU/Linux, existem as seguintes implementações:

-  `LUKS <http://luks.endorphin.org/>`_ - Linux Unified Key Setup: um
   possível novo padrão para criptografia em disco no GNU/Linux; baseado
   na especificação `TKS1 <http://clemens.endorphin.org/publications>`_
   e possui suporte ao `dm-crypt <http://www.saout.de/misc/dm-crypt/>`_.

-  `eCryptfs <http://ecryptfs.sourceforge.net/>`_: esquema bem prático
   de criptografia em disco, é o padrão mais recente desta lista.

Estas são implementações um pouco mais antigas, algumas já em desuso ou
com o desenvolvimento interrompido:

-  `loop-aes <http://loop-aes.sourceforge.net/>`_: uma alternativa às
   implementações oficiais do Linux
-  `Phonebook <http://www.freenet.org.nz/phonebook/>`_: implementação da
   chamada ''deniable encryption'', que é a possibilidade do usuário
   revelar apenas alguns pedaços da informação criptografada caso ele
   seja intimidado a fazê-lo (por exemplo, no caso de tortura); isso é
   feito através de um esquema de camadas, cada uma delas contendo seu
   próprio conteúdo criptografado.
-  `cryptoloop <http://www.tldp.org/HOWTO/Cryptoloop-HOWTO/>`_: a
   tecnologia ainda em uso mas já obsoleta
-  `CryptoFS <http://reboot.animeirc.de/cryptofs/>`_: utiliza o `Linux
   Userland FileSystem <http://lufs.sourceforge.net/lufs/>`_ para
   criptografia; um diretório fica com os arquivos criptografados e o
   acesso aos dados é feito através do ponto de montagem.
-  `PPDD <http://linux01.gwdg.de/~alatham/ppdd.html>`_: cria um
   dispositivo que criptografa automaticamente os dados numa partição,
   conceito semelhante ao dm-crypt, mas este caso não utiliza o
   device-mapper.
-  `CFS <http://www.crypto.com/software/>`_: sistema de arquivos
   criptografado que utiliza o NFS como interface.
-  `TCFS <http://www.tcfs.it/>`_: Transparent CFS (outra implementação
   do CFS).
-  `EncFS <http://arg0.net/users/vgough/encfs.html>`_: sistema de
   arquivos criptografado em nível de usuário, utilizando o
   `FUSE <http://arg0.net/users/vgough/encfs.html>`_. Possui diversos
   problemas de segurança.
-  `StegFS <http://www.mcdonald.org.uk/StegFS/>`_: um sistema de
   arquivos esteganográfico.

Cada uma delas tem suas `vantagens e
desvantagens <https://wiki.boum.org/TechStdOut/LinuxCryptoFS>`_. Para o
sistema ficar transparente, existe o
`pam\_mount <http://www.flyn.org/projects/pam_mount/index.html>`_, que
monta seu sistema de arquivos criptografado automaticamente após você
entrar com seu usuário e desmonta logo que você sai.
