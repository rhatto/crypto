Criptografia
============

`PDF <_static/criptografia.pdf>`_ | `EPUB <_static/criptografia.epub>`_

A criptografia é o método de codificar mensagens de modo a garantir que
só a pessoa que tenha em mãos o código correto possa lê-la.

Para que serve a criptografia? Em resumo, ela serve para

- Proteger informações
- Preservar a privacidade das pessoas
- Permitir a autenticidade de informações (assinatura digital)

Proteger informações significa que você pode escolher quem acessa suas
informações. Preservar a privacidade das pessoas quer dizer que quem não
estiver autorizado a acessar suas informações não conseguirá
decifrá-las. Permitir a autenticidade de informações evita que falem em
seu nome coisas que você não disse.

.. toctree::
   :maxdepth: 1

   openpgp
   instalando
   texto
   internet
   listas
   chat
   arquivos
   nota
   referencias

Baixando
--------

Você pode baixar este guia usando

::

  git clone --recursive https://git.fluxo.info/crypto

Sobre
-----

Este manual não tem sido atualizado o quanto deveria. Apesar de ser um bom roteiro
introdutório sobre criptografias padrão OpenPGP e TLS, ele não cobre aspectos
importantes como anonimato e novos protocolos de mensageria. Assim, consulte
documentação adicional como o `Guia de Autodefesa Digital <https://autodefesa.fluxo.info>`_.

Este guia é baseado num antigo manual de criptografia `disponível originalmente
na documentação do Indymedia
<http://docs.indymedia.org/view/Sysadmin/GnuPGpt>`_, originalmente escrito por
Pietro Bastardi (pietro em bastardi.net) e radicalmente modificado por Silvio
Rhatto (rhatto em riseup.net).  Algumas modificações também foram adicionadas
por Luis (luis em riseup.net).

`Copyright <_static/LICENSE>`_ (c) 2002-2017 Silvio Rhatto. É garantida a permissão para
copiar, distribuir e/ou modificar este documento sob os termos da
Licença de Documentação Livre GNU (GNU Free Documentation License),
Versão 1.2 ou qualquer versão posterior publicada pela Free Software
Foundation; sem Seções Invariantes, Textos de Capa Frontal, e sem Textos
de Quarta Capa. Uma cópia da licença é incluída na seção intitulada
"`GNU Free Documentation License <https://www.gnu.org/licenses/fdl.html>`_\ ".

Mantido por `ISO 1312 <https://opsec.fluxo.info>`_.
