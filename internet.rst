Criptografia e internet
=======================

Criptografia e correio eletrônico
---------------------------------

O uso mais frequente da criptografia é no envio e recebimento de emails.
Uma vez que os pacotes de informação são transmitidas de servidor em
servidor pela internet até chegar no computador de destino, qualquer
pessoa pode monitorar esses pacotes e obter seu conteúdo. Utilizando a
criptografia assegura que apenas o destinatário comprenderá o conteúdo
da mensagem.

No caso do correio eletrônico as coisas são ainda um pouco mais
complicadas, pois não existe dificuldade nenhuma em enviar emails
falsos. Isso não é uma vulnerabilidade, mas sim uma características do
serviço de email. Qualquer pessoa pode acessar um servidor de email e
redigir uma mensagem em nome do Presidente da República. Basta que você
configure seu programa de email para que envie suas mensagens como
presidente@brasil.govSTOPSPAM.br. Isso é possível devido à própria
arquitetura do sistema de email existente na internet.

Além disso, um email passa por muitos servidores até chegar ao seu
destino. Se a mensagem não estiver criptografada, ela pode ser
facilmente interceptada por terceiros. Isso que estou falando não são
falhas do sistema de email, mas sim características deles. Vejamos com
um pouco mais de detalhe como tudo isso funciona.

Na internet existem três tipos de programas de email: os programas do
tipo MTA (Mail Transport Agent), os MDA (Mail Delivery Agent) e ou MUA
(Mail User Agent).

Os Mail Transport Agents (ou Agentes de Transporte de Emails) são
aqueles programas que enviam a mesagem de email para o servidor de
destino, que pode ser, por exemplo, o servidor de emails do provedor de
internet usado pelo destinatário da mensagem. Por causa do protocolo que
os MTAs utilizam, eles são mais conhecidos como servidores SMTP (Simple
Mail Transfer Protocol).

Já os MDAs (Agentes de Entrega de Email) são os programas que enviam a
mensagem até o usuário de destino, e para que este usuário possa receber
a mensagem ele deve utilizar um programa do tipo MUA, que são os
programas de email propriamente ditos. Os servidores de entrega de email
também são conhecidos como servidores POP ou IMAP. Já os clientes de
email (MUAs) podem ser tanto um site de Webmail como os programas do
tipo Mail (MacOSX), Mozilla Mail, etc.

Parece complicado?

O caminho que uma mensagem de correio eletrônico percorre é o seguinte:
o remetente da mensage utiliza seu respectivo cliente de email (MUA)
para enviar a mensagem até o seu programa MTA, que pode ser o servidor
SMTP do seu provedor ou até mesmo ser um programa rodando no computador
do remetente. Esse MTA por sua vez envia essa mensagem até o MTA do
usuário de destino. Quando o destinatário verificar suas novas
mensagens, basta que ele utilize seu cliente de email (MUA) para se
conectar até seu servidor POP ou IMAP (MDA) e receber suas mensagens.

Por exemplo, suponha que um usuário do provedor remetente.br deseja
enviar uma mensagem para destinatario@destinatarioSTOPSPAM.br. O
remetente envia o email para o servidor de SMTP do provedor
remetente.br, que por sua vez manda a mensagem para o SMTP de
destinatario.br. Agora é só o destinatário se conectar ao programa POP,
IMAP ou Webmail do provedor destinatario.br para receber a mensagem.

::

    remetente > smtp.remetente.br > smtp.destinatario.br > pop3.destinatario.br > destinatário
       (mua)         (mta)                 (mta)                  (mda)               (mua)

Em todo esse processo não existe verificação do email de origem da
mensagem, isto é, uma vez que o remetente se conectou em
smtp.remetente.br ele pode escolher qualquer email de origem. Ele pode
escrever a mensagem como destinatario@destinatarioSTOPSPAM.br,
ficticio@ficticio.comSTOPSPAM.br (não precisa nem ser um email válido).
Isso acontece porque normalmente os servidores STMP só requerem usuário
e senha para serem utilizados e eles não fazem nenhuma verificação de
destinatário. Às vezes esses servidores estão desconfigurados e não
precisam nem de senha para serem utilizados, sendo então muito usados
para SPAM (mala direta). Um outro detalhe importante é que qualquer
pessoa pode rodar seu próprio programa de SMTP e com isso enviar
mensagens em nome de outras pessoas.

Já os servidores tipo MDA (pop3, imap, webmail) requerem usuário senha
para fornecerem o acesso à uma conta de email, o que impede as pessoas
de receberem o email de outras pessoas.

Se você quiser testar o quanto o sistema de correio eletrônico é frágil
nesse aspecto da verificação de endereços, experimente utilizar uma
ferramenta de email anônimo, como o http://anony.co.uk/ ou o
http://email.bonloup.org/. Basta preencher o formulário, escolhendo
inclusive o remetente da mensagem, para enviar um email anônimo.

Como na internet todas as informações passam de servidor em servidor até
chegar no seu destino, as mensagens de email são naturalmente
"interceptadas" por outros servidores. Se essa mensagem passar por algum
servidor controlado por pessoas maliciosas, as mensagens de email podem
ser lidas com muita facilidade.

Resumindo, qualquer pessoa pode escrever um email com o endereço de
email de outra pessoa, mas dificilmente conseguirá receber o email de
outras pessoas. No entanto, é possível que mensagens de email sejam
interceptadas enquanto estiverem indo para o seu destino, e se ela não
estiver corretamente criptografada qualquer pessoa pode interpretá-la.

Assim, além de criptografar suas as mensagens, é muito útil assinar com
sua chave pública as mensagens de email que você envia. Assim as pessoas
só confiarão nas mensagens enviadas com o seu endereço se elas estiverem
assinadas com sua chave privada. Por isso, se você não usar
criptografia, você não terá como provar que

-  As mensagens de email que você não enviou mas que alguém enviou em
   seu nome não são realmente suas
-  As mensagens que você enviou em seu nome são realmente suas

Se você adotar a assinatura de mensagens como padrão, você consegue
facilmente provas os dois itens anteriores.

Conexão segura: criptografia na rede
------------------------------------

Vou repetir o que escrevi numa seção anterior: Uma vez que os pacotes de
informação são transmitidas de servidor em servidor pela internet até
chegar no computador de destino, qualquer pessoa pode monitorar esses
pacotes e obter seu conteúdo. Utilizando a criptografia assegura que
apenas o destinatário comprenderá o conteúdo da mensagem.

Se você estiver visitando um site e em algum momento precisar entrar com
uma senha ou qualquer outra informação a ser enviada via formulário,
terceiros podem interceptar essa informação e se ela não estiver
criptografada, pode ser interpretada por qualquer um.

Para contornar esse problema foram criados diversos protocolos que
utilizam criptografia pela internet em tempo real. Por exemplo, quando
navegamos na Web nosso navegador utiliza o protocolo HTTP (`HiperText
Transfer Protocol <http://www.radiolivre.org/node/128/#Protocolo>`_ -
Protocolo de Transferência de Hipertexto), que não suporta nenhum tipo
de criptografia. Já o HTTPS (Secure HTTP - HTTP Seguro) foi feito para
navegarmos na web de forma um pouco mais seguira. Vejamos como funciona:

Quando você usa um nagevador web (`Mozilla <http://www.mozilla.org>`_,
por exemplo) e se conecta num site utilizando o protocolo de conexão
segura (HTTPS), seu navegador e o servidor do site trocam
automaticamente suas respectivas chaves públicas e então é iniciada uma
transmissão de informações criptografadas. '''ATENÇÃO:''' seu navegador
não vai enviar ao servidor a chave pública que você criou, mas sim uma
chave própria criada automaticamente pelo seu navegador de tal forma que
você não precisa de nenhum programa adicional de criptografia (nem mesmo
o GPG). Tudo isso é feito de forma praticamente transparente ao usuário.

A pergunta natural é: como podemos confiar que o site que estamos
acessando realmente é o site que ele diz ser; em outras palavras, existe
um jeito de confiarmos na chave pública do site?

De uma forma parecida como quando trocamos a impressão digital de nossa
chave pública com a de outras pessoas, o site com conexão segura enviará
para o seu navegador um certificado de autenticidade, que é uma espécie
de assinatura da chave pública do site emitida por uma autoridade
certificadora. Uma autoridade certificadora é qualquer pessoa,
organização ou empresa que "assine" certificado de autenticidade. A
Autoridade Cerificadora (ou Certificate Authority) mais conhecida é a
`CAcert.org <http://www.cacert.org>`_, uma organização sem fins
lucrativos que valida tais certificados.

Tudo que o usuário precisa fazer é

-  Confiar na autoridade certificadora
-  Instalar o certificado da autoridade certificadora
-  Confiar que o certificado instalado é o certificado verdadeiro dessa
   autoridade, bastando para isso que você verifique a impressão digital
   desse certificado.

Por exemplo, você pode tentar baixar o arquivo
http://www.cacert.org/cacert.crt, que contém o certificado principal da
Cacert.org. Na maioria dos navegadores, abrirá uma janela perguntando se
você aceita a Cacert como uma autoridade certificadora.

::

    Inserir imagem: cacert.jpg

Se você tem dúvidas para aceitar, veja os detalhes do certificado,
conferindo se fingerprints do certificado corresponde àqueles que a
autoridade certificadora divulga em seu site.

::

    Inserir imagem: cacert2.jpg

O processo da conexão segura via web é o seguinte: suponha que você
esteja visitiando o site lists.indymedia.org. Se você quiser fazer isso
via conexão segura, você digitará https://lists.indymedia.org ao invés
de http://lists.indymedia.org (note a diferença de https ao invés de
http).

Na primeira vez que você acessar um site via conexão segura, caso você
não tenha instalado o certificado da autoridade certificadora que
validou o certificado desse site, seu navegador lhe informará a respeito
e perguntará se você quer mesmo assim aceitar o certificado desse site,
conforme mostra a figura abaixo.

::

    Inserir imagem: https.jpg

Agora você tem três opções:

-  Confiar nesse certificado e clicar em Ok ou
-  Ir até o site da Autoridade Certificadora que emitiu o certificado do
   site, instalar o certificado da autoridade e em seguida voltar para o
   site que você estava tentando acessar via conexão segura ou então
-  Desistir de acessar o site smile

Se clicarmos em ''Examine certificate...'' (Examinar certificado...),
aparecerá uma janela como mostra a figura abaixo. Nela, as impressões
digitais do certificado do site http://lists.indymedia.org estão em SHA1
Fingerprint e MD5 Fingerprint. O campo Issued to (emitido para) informa
para qual site e organização o certificado, enquanto que o campo Issued
by (emitido por) diz qual foi a autoridade certificadora que emitiu esse
certificado. O campo Validity (validade) mostra qual é o prazo de
validade do certificado.

::

    Inserir imagem: https2.jpg

Quando passa o prazo de validade, o certificado expira e o navegador não
mais o reconhecerá como válido e ao acessar o site aparecerá uma janela
mais ou menos como a abaixo, avisando que o certificado expirou e
portando a conexão segura pode estar comprometida.

::

    Inserir imagem: certificate.jpg

Da mesma forma como é possível navegar pela web com conexão segura,
também é possível utilizar outros serviços na internet, como ''ftp''
(transmissão de arquivos) e ''irc'' (bate papo), de forma criptografada.
