Referências
===========

Esse Guia foi escrito para conter tudo o que uma pessoa precisa para
usar a criptografia no dia-a-dia. Contudo, algumas coisas ficaram de
fora ou pouco aprofundadas. Aqui encontram-se alguns guias com maior
detalhamento.

-  Uma ótima referência em português para criptografia no Linux (`Modo
   Texto <nota>`_ apenas) encontra-se no `Guia Foca
   Linux <http://focalinux.cipsga.org.br/guia/avancado/ch-d-cripto.htm>`_,
   de Gleydson Mazioli da Silva.
- `A Practical Introduction to GPG in Windows, de Brendan Kidwell <http://www.glump.net/archive/000060.php>`_.
- `Using multiple subkeys in GPG, de Adrian von Bidder <http://fortytwo.ch/gpg/subkeys/>`_.
- `OpenPGP Best Practices <https://help.riseup.net/en/security/message-security/openpgp/best-practices>`_.
- `Manual de privacidade GNU <http://web.archive.org/web/20080505054741/http://br.geocities.com/sdiasneto/gnupg/gnupg-manual-ptbr.html>`_.

Do próprio sítio do GNU Privacy Guard podemos destacar (muita coisa só
em inglês):

- `The GNU Privacy Handbook, de John Michael Ashley <http://www.gnupg.org/gph/en/manual.html>`_.
- `Perguntas mais frequentes sobre o GNU PG, documento mantido por David D.
   Scribner <http://www.gnupg.org/(en)/documentation/faqs.html>`_.
- `GNU PG Mini-HOWTO, por Brenno J.S.A.A.F. de Winter, Michael Fischer v. Mollard e Arjen
   Baart <http://webber.dewinter.com/gnupg_howto/english/GPGMiniHowto.html>`_.
- `GPG no MacOSX <http://fiatlux.zeitform.info/en/instructions/pgp_macosx.html>`_.
- `Ritual de assinatura de chaves no Debian <http://www.debian.org/events/keysigning>`_.
