Usando os GPG em modo texto
===========================

Esta seção trata do uso diário da criptografia, tanto em modo texto
quando em modo gráfico. Se você ainda não o fez, `leia nota sobre modo
texto e modo gráfico <nota>`_. Ações como criar e gerenciar chaves,
assinar, verificar assinaturas, criptografar e descriptografar serão
tratadas adiante.

As instruções de como utilizar o GPG no Modo Texto funcionam tanto se
você usa GNU/Linux quanto Windows, MacOSX ou qualquer outro sistema
operacional.

Como criar seu par de chaves
----------------------------

Abra um terminal. Em seguida:

1. Digite "gpg --gen-key"

2. Nas três primeiras perguntas apenas aperte enter. É seguro usar as
   opções padrão do GPG:

   gpg (GnuPG) 1.2.3; Copyright (C) 2003 Free Software Foundation, Inc.
   This program comes with ABSOLUTELY NO WARRANTY. This is free
   software, and you are welcome to redistribute it under certain
   conditions. See the file COPYING for details.

   Por favor selecione o tipo de chave desejado:

   (1) DSA e ElGamal (padrão)
   (2) DSA (apenas assinatura)
   (3) RSA (apenas assinatura) Sua opção? 1 O par de chaves DSA terá
       1024 bits. Prestes a gerar novo par de chaves ELG-E. tamanho
       mínimo é 768 bits tamanho padrão é 1024 bits tamanho máximo
       sugerido é 2048 bits Que tamanho de chave você quer? (1024) 1024
       O tamanho de chave pedido é 1024 bits

3. Na terceira pergunta você deve escolher por quanto tempo suas chaves
   serão válidas. Você pode escolher chaves que expiram em poucos dias
   para usá-las de teste ou chaves que expiram em anos ou que não
   expiram. Para uma chave que expira em n dias, digite nd (por exemplo,
   10d para dez dias), para n semanas digite nw (por exemplo, 2w para
   duas semanas), para n meses, use nm (por exemplo, 5m para cinco
   meses), para n anos use ny (por exemplo, 1y para hum ano). Se você
   quer uma chave que não expira, digite 0 (zero). Veja o exemplo:

   Por favor especifique por quanto tempo a chave deve ser válida. 0 =
   chave não expira = chave expira em n dias w = chave expira em n
   semanas m = chave expira em n meses y = chave expira em n anos A
   chave é valida por? (0) 6m Key expira em Dom 15 Ago 2004 22:44:56 BRT

Depois dessa data a chave não mais será válida. É possível, no entanto,
alterar a data de validade da chave depois que ela foi criada.

4. Será perguntado se está tudo correto. Se você seguiu tudo direito até
   aqui pode digitar "y" ou "s" caso você esteja usando uma versão
   traduzida para o português e apertar ENTER.

   Está correto (s/n)? s

5. Digite seu nome.

   Você precisa de um identificador de usuário para identificar sua
   chave; o programa constrói o identificador a partir do Nome Completo,
   Comentário e Endereço Eletrônico desta forma: "Heinrich Heine (Der
   Dichter) heinrichh@duesseldorf.de"

   Nome completo: Truta

6. Digite seu e-mail.

   Endereço de correio eletrônico: truta@uzma.net

7. Não digite um comentário a não ser que você queira ter mais de uma
   chave como por exemplo uma para usar no trabalho, outra para usar em
   casa.

Comentário:

8. Verifique se tudo que você digitou está certo. Se estiver digite "o"
   e aperte enter.

   Você selecionou este identificador de usuário: "Truta truta@uzma.net"

   Muda (N)ome, (C)omentário, (E)ndereço ou (O)k/(S)air? o

9. Digite sua frase-senha. '''IMPORTANTE''': sem essa frase-senha você
   não poderá descriptografar o que lhe enviarem! Por isso use uma FRASE
   que você não esqueceria, porém que seja muito difícil de alguém
   adivinhar. Use letra minúsculas e maiúsculas, elas serão
   diferenciadas, pontuação (,.!?) e espaços. Você não verá o que
   digita.

Você precisa de uma frase secreta para proteger sua chave.

::

    Digite a frase secreta:

10. Repita sua frase-senha para confirmar que você não cometeu nenhum
    erro ao digitá-la. De novo você não verá o que digita.

    Repita a frase secreta:

11. Se você digitou as duas frases-senha iguais suas chave será gerada.
    Digite no teclado e mova o mouse aleatóriamente para ajudar o
    programa a gerar as chaves.

    Precisamos gerar muitos bytes aleatórios. É uma boa idéia realizar
    outra atividade (digitar no teclado, mover o mouse, usar os discos)
    durante a geração dos números primos; isso dá ao gerador de números
    aleatórios uma chance melhor de conseguir entropia suficiente.
    ++++++++++.++++++++++++++++++++.+++++.+++++.+++++.+++++++++++++++.
    chaves pública e privada criadas e assinadas. chave marcada como de
    confiança absoluta

    pub 1024D/90716386 2004-02-18 Truta truta@uzma.net Impressão da
    chave = 27FF 8594 D529 B428 8E7F 9A81 C127 6A77 9071 6386 sub
    1024g/7C866836 2004-02-18[expira: 2004-08-16]

    12. Digite gpg --fingerprint e você verá a sua chave e sua
        "impressão digital" algo do tipo:

    pub 1024D/90716386 2004-02-18 Truta truta@uzma.net Impressão da
    chave = 27FF 8594 D529 B428 8E7F 9A81 C127 6A77 9071 6386 sub
    1024g/7C866836 2004-02-18[expira: 2004-08-16]

A sua "impressão digital" é um número hexadecimal (na base 16) de 40
dígitos, sendo que os 8 últimos são a identificação da sua chave, o que
diferencia a sua chave das outras.

Como compartilhar sua chave pública 
------------------------------------

1. Para que o GPG seja de alguma utilidade você terá que mandar sua
   chave pública para aqueles que vão te enviar mensagens
   criptografadas. Para isso extraia sua chave pública digitando:

::

   gpg --export --armor -o chave.asc seu@email

Sua chave pública estará no arquivo chave.asc no diretório atual. Dê uma
olhada nela. Você também pode exportar chaves públicas de outras
pessoas, desde que elas estejam no seu chaveiro. Basta usar o email
delas ao invés do seu@email.

2. Existem basicamente dois métodos para que você transmita sua chave
   pública:

-  Você enviar sua chave para uma pessoa (por email, disquete, cd,
   etc.).
-  Você enviar sua chave pública para um servidor e cada pessoa que
   quiser usá-la baixa a chave do servidor.

Enviar sua chave para um servidor é o meio mais cômodo de compartilhar
sua chave com as pessoas que vão enviar mensagens criptografadas para
você. Dessa forma você não precisa ficar mandando suas chave pública pra
todo mundo.

Para mandar sua chave para um servidor de chaves, use o comando

::

    gpg --keyserver servidor.de.chaves --send-keys nome

Se você não souber sob quais nomes suas chaves públicas estão
registradas, liste-as com o comando

::

    gpg --list-keys

Por exemplo, se o servidor for keys.indymedia.org e o nome da sua chave
for truta, o comando para exportá-la será

::

    gpg --keyserver keys.indymedia.org --send-keys truta

É importante observar que você pode exportar qualquer chave pública do
seu chaveiro, que não precisa ser necessariamente sua.

Como adicionar uma chave pública de alguém na sua lista 
--------------------------------------------------------

Vamos supor que o arquivo que contém a chave pública de alguém é
chama-se truta.asc. Para incluir esta chava na sua lista, basta dar o
comando:

::

    gpg --import truta.asc

Para importar uma chave pública de um servidor, você precisa saber qual
é o servidor e qual é o ID da chave. Com isso em mãos, basta digitar

::

    gpg --keyserver servidor.de.chaves --recv-keys id-da-chave

que a chave pública será importada. Por exemplo, se o servidor que você
estiver usando for o keys.indymedia.org, e o ID da chave for B9A88F6F,
seu comando será

::

    gpg --keyserver keys.indymedia.org --recv-keys B9A88F6F

Se você não tiver o ID da chave que você quer adicionar, primeiro faça
uma busca no servidor de chaves. Por exemplo, se eu quiser saber qual é
o ID da chave do Pietro, basta que eu dê o comando

::

    gpg --keyserver keys.indymedia.org --search-keys pietro@indymedia.org

A saída provável será

::

    gpg: a procurar por "pietro@indymedia.org" no servidor HKP keys.indymedia.org
    Keys 1-1 of 1 for "pietro@indymedia.org"
    (1)     Pietro Ferrari <pietro@indymedia.org>
              1024 bit DSA key D75301BF, created 2002-02-23
    Enter number(s), N)ext, or Q)uit >

Assim você obtém o ID da chave do Pietro, que é D75301BF. Aí é só
digitar Q para sair da busca e em seguida adicionar a chave, usando o
comando

::

    gpg --keyserver keys.indymedia.org --recv-keys D75301BF

Você poderia, ao invés de procurar pelo email do Pietro, procurar apenas
pelo nome dele, usando o comando

::

    gpg --keyserver keys.indymedia.org --search-keys pietro

É importante ressaltar que você só encontrará a chave desejada desde que
a pessoa que você procura deixou a chave naquele servidor.

Depois de adicionar uma chave pública de terceiros, efetue os
procedimentos na seção `Verificando Impressões Digitais e Assinando
Chaves <#verificando>`_.

Listando seu chaveiro
---------------------

Você pode ver todas as chaves do seu chaveiro - incluindo seu par de
chaves pública e privada - digitando

::

    gpg --list-keys

A saída é algo do tipo

::

    /users/alice/.gnupg/pubring.gpg
    ---------------------------------------
    pub  1024D/BB7576AC 1999-06-04 Alice (Judge) <alice@cyb.org>
    sub  1024g/78E9A8FA 1999-06-04

Como assinar mensagens e arquivos
---------------------------------

Existem muitas maneiras de assinar mensagens ou arquivos. A primeira
delas consiste em entrar no GPG para escrever sua mensagem. No seu
terminal, digite:

::

    gpg --clearsign

E entre com sua senha. A opção clerasign pede ao GPG para que ele crie
uma assinatura utilizando texto comum, isto é, codificado em caracteres
ASCII (legíveis ao usuário). Não sabe o que é ASCII ou texto comum?
Então veja uma nota
`aqui <http://docs.indymedia.org/view/Sysadmin/GuiaHtml#Texto_puro>`_.

Depois de entrar com sua senha, o GPG estará esperando para que você
escreva sua mensagem. Escreva sua mensagem - "Testando essa parada!",
por exemplo - e após escrevê-la, pule uma linha e digite simultaneamente
as teclas Ctrl e D do seu teclado. Isso fará com que o GPG crie uma
assinatura da sua mensagem. O resultado deve ser algo do tipo

::

    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA1

    Testando essa parada!
    -----BEGIN PGP SIGNATURE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    iD8DBQFAMQPyvSy1nGtWZ3cRAld5AJ4sIOed+/kFmFwARMngu+EF73DMCQCgz59l
    9okdJxyGs65FL9SycVmVMNg=
    =WC+w
    -----END PGP SIGNATURE-----

Certo. Assinei minha mensagem. E agora, o que faço com isso? Bom, se
você quiser mandá-la pra alguém, copie e cole todo esse texto, desde o
``-----BEGIN PGP SIGNED MESSAGE-----`` até o
``-----END PGP SIGNATURE-----`` e cole no corpo da sua mensagem de
email. Se o destinatário tiver sua chave pública, ele poderá facilmente
verificar se a assinatura confere. Mas caso você queira guardar essa
mensagem assinada, basta copiar tudo e colar num arquivo.

'''ATENÇÃO:''' se porventura você alterar essa mensagem, sua respectiva
assinatura perderá seu valor. Se você quiser alterar a mensagem, faça e
depois assine novamente.

Vejamos agora uma segunda maneira de assinar mensagens. Escreva seu
texto num arquivo, por exemplo no texto.txt. Em seguida, digite no seu
terminal:

::

    gpg --clearsign texto.txt

Depois de entrar com sua senha, o GPG escreverá a mensagem assinada no
arquivo texto.txt.asc. Com esse procedimento é possível assinar qualquer
tipo de arquivo, e o formato da mensagem assinada será em texto simples
(ASCII). Para criar uma assinatura separada da mensagem - a mensagem no
arquivo texto.txt e apenas a assinatura no arquivo arquivo.txt.asc - é
só digitar:

::

    gpg -a --detach-sig texto.txt

As mensagens e assinaturas armazenadas em texto simples ocupam mais
espaço do que se estivessem no formato "binário". Se você quiser guardar
a assinatura num formato binário, que é pouco amigável para ser
visualizada num editor de textos mas que ocupa pouco espaço - basta
digitar

::

    gpg --sign texto.txt

E a mensagem assinada estará no arquivo texto.txt.gpg. Além de assinar,
a opção sign compacta a mensagem, ocupando menos espaço ainda. Essa
forma de assinar não é boa para trocar mensagens com outras pessoas, já
que não está num formato legível. Prefira sempre a opção clearsign

Como verificar mensagens assinadas
----------------------------------

Uma vez que você tenha recebido uma mensagem ou arquivo assinado, você
terá de verificar se a assinatura está correta. Existem várias maneiras
de fazer isso.

A maneira mais simples é a que se segue: você recebeu uma mensagem como
essa:

::

    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA1

    Alow!
    -----BEGIN PGP SIGNATURE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    iD8DBQFAMU1TvSy1nGtWZ3cRAtz2AJ41a1dGqGwb0wT+kz4WoFq9/4+RoQCfZH29
    0gPrLalgr5OrC4gC9LahbOw=
    =d2qf
    -----END PGP SIGNATURE-----

Para verificar essa assinatura, copie todo esse texto, desde o
'''-----BEGIN PGP SIGNED MESSAGE-----''' até o '''-----END PGP
SIGNATURE-----''', digite, no seu terminal

::

    gpg

depois, cole todo o texto e em seguida digite simultaneamente as teclas
Ctrl e D do seu teclado. Se tudo der certo, o GPG detectará que trata-se
de uma mensagem assinada e verificará sua validade. No caso da mensagem
acima, termos:

::

    gpg: Assinatura feita em Seg 16 Fev 2004 20:08:03 BRT usando DSA, ID da chave 6B566777
    gpg: Assinatura correta de "Silvio Rhatto <rhattoEMriseup.net>"

Se você recebeu uma mensagem assinada num arquivo, por exemplo o
mensagem.txt.asc, basta digitar

::

    gpg --verify mensagem.txt.asc

Que a assinatura será verificada. Se você recebeu uma mensagem com a
assinatura num arquivo separado - mensagem.txt e mensagem.txt.asc, por
exemplo - digite

::

    gpg --verify mensagem.txt.asc mensagem.txt

Como codificar uma mensagem para alguém
---------------------------------------

Assim como para assinar mensagens, existem várias maneiras de se
criptografar uma mensagem. A primeira delas consiste em digital sua
mensagem no próprio gpg. Vamos lá. No terminal, digite:

::

    gpg -e -a -r email@da.pessoa

E em seguida escreva sua mensagem. Quando terminal, pule uma linha e em
seguida digite simultaneamente as teclas Ctrl e D do seu teclado. O GPG
então fornecerá a mensagem codificada para o usuário cujo email é
email@daSTOPSPAM.pessoa. Agora é só copiar todo o texto, desde
``-----BEGIN PGP MESSAGE-----`` até ``-----END PGP MESSAGE-----`` e
colar no seu email ou arquivo!

Para uma encriptar e compactar um arquivo, digite:

::

    gpg -r nome-do-usuário -e mensagem.txt

O arquivo de saída será ``mensagem.txt.gpg``

Se você quiser apenas encriptar e que o arquivo de saída possa ser
enviado por email - texto comum, isto é, codificado em caracteres ASCII
(legíveis ao usuário), digite

::

    gpg -r nome-do-usuário -e -a mensagem.txt

E o arquivo de saída será mensagem.txt.asc

Não sabe o que é ASCII ou texto comum? Então veja uma nota
`aqui <http://docs.indymedia.org/view/Sysadmin/GuiaHtml#Texto_puro>`_.

Como decodificar uma mensagem que enviaram para você
----------------------------------------------------

A maneira mais simples de desencriptar uma mensagem é colá-la
diretamente no GPG. Suponha que você tenha recebido a mensagem

::

    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    hQEOA3v8xQeh8DSxEAP8DGLGac9OdZzX7KxRqpkaYuJ/8NVN5AyhtQKZwRogZwwZ
    19g/teTPQLYwgcCLQoDKxsnX3lBGEzjAGxXme6aqcJetD0sXcULtap99AfpJqIO/
    lCDjxFqNRP45UwnnbafakiVDTj71H6jDi6UMP4c+F/JJL65Q9ROHW08kOB1IM1sD
    /RsQu1pqZl/X8PRZyVdLSwpzGpR5uaxA837f+Zl0+1Fh2DhoiE2AxnLXdwlMlRtK
    SrpqBdWifULoX46E1+DOd128e24K74d+utMux4uk8t9Lb0D5C8RDfShKHoLJIwul
    fhEmtN8bo2Kmg/z8sWnDjW3Ik3opVtsTPNPQQh1J9GV40lUBViB6IzhkXhhNZ4ae
    qexbLilOVwJczLa3y3UmwkiXcs92k6thpUCIJjyeRTtpey2LKdVLHLd1o5ti8/or
    nqYoq1eXHcVOckmfxH3Uq8ZAEX6bzSKc
    =g5JE
    -----END PGP MESSAGE-----

Tudo que você precisa fazer é digitar

::

    gpg

no seu terminal e em seguida colar a mensagem. O GPG detectará que
trata-se de uma mensagem privada e pedirá pela sua senha, mais ou menos
assim:

::

    gpg: Vá em frente e digite sua mensagem ...
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    hQEOA3v8xQeh8DSxEAP8DGLGac9OdZzX7KxRqpkaYuJ/8NVN5AyhtQKZwRogZwwZ
    19g/teTPQLYwgcCLQoDKxsnX3lBGEzjAGxXme6aqcJetD0sXcULtap99AfpJqIO/
    lCDjxFqNRP45UwnnbafakiVDTj71H6jDi6UMP4c+F/JJL65Q9ROHW08kOB1IM1sD
    /RsQu1pqZl/X8PRZyVdLSwpzGpR5uaxA837f+Zl0+1Fh2DhoiE2AxnLXdwlMlRtK
    SrpqBdWifULoX46E1+DOd128e24K74d+utMux4uk8t9Lb0D5C8RDfShKHoLJIwul
    fhEmtN8bo2Kmg/z8sWnDjW3Ik3opVtsTPNPQQh1J9GV40lUBViB6IzhkXhhNZ4ae
    qexbLilOVwJczLa3y3UmwkiXcs92k6thpUCIJjyeRTtpey2LKdVLHLd1o5ti8/or
    nqYoq1eXHcVOckmfxH3Uq8ZAEX6bzSKc
    =g5JE
    -----END PGP MESSAGE-----
    Você precisa de uma frase secreta para desbloquear a chave secreta do
    usuário: "Silvio Rhatto <rhatto@riseup.net>"
    chave de 1024-bit/ELG-E, ID A1F034B1, criada em 2003-06-20 (ID principal da chave 6B566777)

    Digite a frase secreta: 

Digite sua senha e em seguida pressione simultaneamente as teclas Ctrl e
D do seu teclado que a mensagem secreta irá aparecer!

Se alguém lhe enviou uma mensagem.txt.asc, basta dar este comando para
desencriptá-la e guardar o texto em mensagem.txt:

::

    gpg -d mensagem.txt.asc > mensagem.txt

Esse comando funciona tanto para arquivos criptografados em texto
simples quanto em formato "binário".

Verificando Impressões Digitais e Assinando Chaves 
---------------------------------------------------

Conforme você viu na seção `O limite da confiabilidade <#limite>`_, é
possível verificar pela impressão digital da chave pública se ela
pertence realmente a quem afirma pertencer.

Suponha que alguém envia uma chave pública e você a `adiciona em seu
chaveiro <#adicionando>`_. Posteriormente você tem a oportunidade de
encontrar ao vivo o suposto dono da chave e ele lhe fornece a impressão
digital da chave num papel.

Chegando em casa, você decide verificar se a impressão digital bate com
a chave pública. No seu terminal, digite

::

    gpg --fingerprint email@da.pessoa

onde email@da.pessoa é o email da pessoa que você encontrou. A impressão
digital da chave será impressa. Confira se ela é idêntica àquela que
você tem anotada. Se elas forem iguais, você pode começar a pensar em
assinar essa chave pública. Aqui mostrarei o procedimento de assinar uma
chave pública e em seguida reproduzirei um trecho do `Guia Foca
Linux <http://focalinux.cipsga.org.br/guia/avancado/ch-d-cripto.htm>`_,
que por sua vez foi retirado da lista debian-user-portuguese EM
lists.debian.org, que trata de modo muito sério a assinatura de chaves.

Digite no seu terminal:

::

    gpg --edit-key email@da.pessoa

Aparecerão informações sobre essa chave. Digite

::

    sign

E digite sua senha. Pronto, a chave estará assinada.

Certo, assinei a chave pública daquela pessoa. E agora, o que faço com
isso? Você pode `exportar a chave pública <#exportando>`_ dessa pessoa -
que será automaticamente exportada com sua assinatura. Você tanto pode
exportá-la num arquivo e enviá-la para essa pessoa quanto mandar essa
chave assinada para um servidor de chaves.

Suponha que Arrelia assinou a chave de Pasqualin,
`exportou-a <exportando>`_ no arquivo pasqualin.asc e enviou-a para
Pasqualin. Quando Pasqualin `adicionar <#adicionando>`_ a chave contida
em pasqualin.asc no seu chaveiro, a assinatura feita por Arrelia da
chave pública de Pasqualin será automaticamente adicionada ao chaveiro
de Pasqualin. Agora, sempre que Pasqualin enviar sua chave pública para
alguém, a assinatura de Arrelia sempre estará presente.

Um outro método para o intercâmbio de assinaturas utiliza os servidores
de chaves. Por exemplo, se a chave pública de Pasqualin estiver
armazenada no servidor chaves.privacidade.net, basta que Arrelia assine
a chave pública de Pasqualin e `exporte-a <#exportando>`_ para esse
servidor para que o servidor adicione a assinatura de Arrelia na sua
cópia da chave pública de Pasqualin. Em seguida, basta que Pasqualin
atualize seu chaveiro, de forma que sua própria chave pública seja
baixada do servidor chaves.privacidade.net para que a assinatura de
Arrelia entre no seu chaveiro.

Aqui segue o texto retirado do `Guia Foca
Linux <http://focalinux.cipsga.org.br/guia/avancado/ch-d-cripto.htm>`_:

::

      Trocando assinaturas de chaves digitais

      Direitos de republicação cedidos ao domínio público, contanto que o texto
      seja reproduzido em sua íntegra, sem modificações de quaisquer espécie, e
      incluindo o título e nome do autor.


      1. Assinaturas digitais
      2. Chaves digitais e a teia tipo de
      problema:  Ao usuário é dado o poder de "assinar" uma chave digital, dizendo
      "sim, eu tenho certeza que essa chave é de fulano, e que o e-mail de fulano é
      esse que está na chave".

      Note bem as palavras "certeza", e "e-mail".  Ao assinar uma chave digital,
      você está empenhando sua palavra de honra que o _nome_ do dono de verdade
      daquela chave é o nome _que está na chave_, e que o endereço de e-mail
      daquela chave é da pessoa (o "nome") que também está na chave.

      Se todo mundo fizer isso direitinho (ou seja, não sair assinando a chave de
      qualquer um, só porque a outra pessoa pediu por e-mail, ou numa sala de
      chat), cria-se a chamada teia de confiança.

      Numa teia de confiança, você confia na palavra de honra dos outros para
      tentar verificar se uma chave digital é legítima, ou se é uma "pega-bobo".

      Suponha que Marcelo tenha assinado a chave de Cláudia, e que Roberto, que
      conhece Marcelo pessoalmente e assinou a chave de Marcelo, queira falar com
      Cláudia.  

      Roberto sabe que Marcelo leu o manual do programa de criptografia, e que ele
      não é irresponsável.  Assim, ele pode confiar na palavra de honra de Marcelo
      que aquela chave digital da Cláudia é da Cláudia mesmo, e usar a chave pra
      combinar um encontro com Cláudia.

      Por outro lado, Roberto não conhece Cláudia (ainda), e não sabe que tipo de
      pessoa ela é. Assim, rapaz prevenido, ele não confia que Cláudia seja uma
      pessoa responsável que verifica direitinho antes de assinar chaves.

      Note que Roberto só confiou na assinatura de Marcelo porque, como ele já
      tinha assinado a chave de Marcelo, ele sabe que foi Marcelo mesmo quem
      assinou a chave de Cláudia.  

      Enrolado? Sim, é um pouco complicado, mas desenhe num papel as flechinhas de
      quem confia em quem, que você entende rapidinho como funciona.

      O uso da assinatura feita por alguém cuja chave você assinou, para validar
      a chave digital de um terceiro, é um exemplo de uma pequena teia de
      confiança.


      3. Trocando assinaturas de chaves digitais com um grupo de pessoas

      Lembre-se: ao assinar uma chave digital, você está empenhando sua palavra de
      honra que toda a informação que você assinou naquela chave é verdadeira até
      onde você pode verificar, _e_ que você tentou verificar direitinho.

      Pense nisso como um juramento: "Eu juro, em nome da minha reputação
      profissional e pessoal, que o nome e endereços de e-mail nessa chave são
      realmente verdadeiros até onde posso verificar, e que fiz uma tentativa real
      e razoável de verificar essa informação."

      Sim, é sério desse jeito mesmo. Você pode ficar muito "queimado" em certos
      círculos se você assinar uma chave falsa, pensando que é verdadeira:  a sua
      assinatura mal-verificada pode vir a prejudicar outros que confiaram em
      você.

      Bom, já que o assunto é sério, como juntar um grupo de pessoas numa sala, e
      trocar assinaturas de chaves entre si?  Particularmente se são pessoas que
      você nunca viu antes? Siga o protocolo abaixo, passo a passo, e sem pular ou
      violar nenhum dos passos.


        1 -  Reúna todos em uma sala, ou outro local não tumultuado, pressa e
             bagunça são inimigas da segurança.

        2 -  Cada um dos presentes deve, então, ir de um em um e:

             2.1 -  Apresentar-se, mostrando _calmamente_ documentação original
                    (nada de fotocópia) comprovando sua identidade.  RG, CPF,
                    passaporte, certidão de nascimento ou casamento, carteira de
                    motorista, cartão de crédito são todos bons exemplos. Só o RG
                    sozinho não é -- tem muito RG falsificado por aí -- mas o RG
                    junto com o cartão de banco já seria suficiente.  Se nenhum
                    documento tiver foto, também não é o bastante.

                    * Se alguém pedir o documento na mão, para verificar
                    direitinho, não leve pro lado pessoal. Deixe a pessoa
                    verificar até esta        4.2 -  As informações não bateram, por isso você não deve assinar a
                    chave.  Se quiser, envie um e-mail avisando que não poderá
                    assinar a chave. Não aceite tentativas de retificação por
                    e-mail ou telefone. Um outro encontro face-à-face, refazendo
                    todos os passos 2.1 e 2.2 é o único jeito de retificar
                    o problema.

             4.3 -  As informações bateram, o que garante que o *nome* está
                    correto. Agora é preciso ter certeza do endereço de e-mail.
                    Para isso, envie uma e-mail *CIFRADA* pela chave que você está
                    testando, para o endereço de e-mail constante na chave.  Nessa
                    e-mail, coloque uma palavra secreta qualquer e peça para o
                    destinatário te responder dizendo qual a palavra secreta que
                    você escreveu. Use uma palavra diferente para cada chave que
                    estiver testando, e anote no papel daquela chave qual palavra
                    você usou.

             4.4 -  Se você receber a resposta contendo a palavra secreta correta,
                    você pode assinar a chave. Caso contrário, não assine a chave -- 
                    o endereço de e-mail pode ser falso.


      Comandos do gpg (GNUpg) correspondentes a cada passo:
        2.2 -       gpg --fingerprint <seu nome ou 0xSuaKEYID>
                    (retorna as informações que devem estar no papel a ser
                    entregue no passo 2.2)

        4.1 -       gpg --receive-key <0xKEYID>
                    (procura a chave especificada nos keyservers)
                    gpg --sign-key <0xKEYID>
                    (assina uma chave)

                    Assume-se que você sabe cifrar e decifrar mensagens. Caso
                    não saiba, ainda não é hora de querer sair assinando chaves.

O trecho acima descreve talvez o método mais rigoroso que alguém poderia
ter para a assinatura de chaves públicas. É lógico que você não precisa
ser tão rígido para assinar chaves dos seus amigos ou pessoas que você
realmente conhece e confia. Para essas, basta trocarem ao vivo as
impressões digitais das chaves públicas e confirmarem seus endereços de
email para que ambos possam assinar as chaves públicas.

Recebendo sua chave assinada
----------------------------

Se alguém assinou sua chave, é conveniente você atualizar sua cópia da
sua chave pública para que ela contenha essa nova assinatura. Você pode
fazer isso de duas maneiras: importando a chave que a pessoa te enviou
pela forma usual, ou seja, utilizando o comando `gpg
--import <#adicionando>`_ ou, caso ela a tenha enviado para um servidor
de chaves, atualizando seu chaveiro de acordo com as últimas modificaçõs
de chaves do servidor.

Para essa segunda opção, basta digitar:

::

    gpg --refresh-keys --keyserver keys.indymedia.org

onde keys.indymedia.org é o servidor de chaves para o qual a pessoa
mandou a chave assinada. Esse comando fará com que todas as chaves
públicas do seu chaveiro - inclusive a sua - sejam atualizadas a partir
das chaves públicas existentes no servidor de chaves. Assim, se alguém
assinou uma chave e a exportou para o servidor de chaves, esse comando
atualizará seu chaveiro substituindo a chave pública antiga pela nova.

É muito interessante dar esse comando periodicamente para atualizar seu
chaveiro, independentemente de alguém ter assinado uma chave. As
atualizações de chaves podem acontecer sem ninguém te avisar.

Confiando em chaves
-------------------

Assinar chaves mostra a outras pessoas que você confia na procedência de
determinadas chaves públicas. Mas pode acontecer de você assinar a chave
de um amigo seu mas não confiar nas chaves que ele assina. Existe uma
maneira de lembrar a você em quais colegas seus você confia quando eles
assinam chaves de outras pessoas, que é o chamado nível de
confiabilidade daquela chave.

Essa informação não é passada a outros usuários. Quando exportada, não
existirá diferença nenhuma se a chave pública foi definida por você com
alto ou baixo nível ente, uma chave pública é considerada válida apenas
se ela for assinada por você. Mas usando o conceito de Teia de
Confiabilidade, o GPG é bem flexível em considerar uma chave válida, por
exemplo, se:

-  Ela foi assinada por você '''ou'''
-  Ela foi assinada por alguém que você confia totalmente '''ou'''
-  Ela foi assinada por três chaves que você confia moderadamente
   '''ou'''
-  Se existe um caminho entre você e a chave pelo qual todas as chaves
   estão assinadas. João assinou a chave de Raimundo, que assinou a
   chave de Maria, cuja chave você assinou; esse caminho permite que o
   GPG considere válida a chave de João, sem que você precise assiná-la.
   Normalmente o número de pessoas nessa corrente, para que a chave
   torne-se válida, não pode ser maior que cinco.

Voce não precisa decorar esse esquema! Uma vez que você seleciona o
nível de confiabilidade de uma chave, o GPG automaticamente recalcula a
validade de todas as chaves do seu chaveiro, usando um método um pouco
mais sofisticado do que o exemplificado acima.

Removendo chaves
----------------

Se você quiser remover a chave pública de alguém, use o comando

::

    gpg --delete-key email@da.pessoa

onde email@da.pessoa é o email da pessoa cuja chave você quer apagar.
Agora, se você quiser remover um par de chaves (pública e privada), use
o comando:

::

    gpg --delete-secret-and-public-key seu@email

onde seu@email é o seu email. Exemplo:

::

    gpg --delete-secret-and-public-key truta@uzma.net
    gpg (GnuPG) 1.2.3; Copyright (C) 2003 Free Software Foundation, Inc.
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. See the file COPYING for details.

    sec  1024D/90716386 2004-02-18   Truta <truta@uzma.net>

      Deletar esta chave do chaveiro? s
      Esta é uma chave secreta! - realmente deletar? s
      pub  1024D/90716386 2004-02-18   Truta <truta@uzma.net>

      Deletar esta chave do chaveiro? s

Mas tome cuidado: uma vez que você apagou seu par de chaves, não há mais
como recuperá-las, ler mensagens criptografas para você ou assinar
mensagens. Lembre-se de revogar sua chave pública antes de cancelá-la
(veja como na próxima seção).

Cancelando um par de chaves
---------------------------

Se você quiser cancelar um par de chaves, por qualquer motivo - alguém
roubou sua chave secreta e sua senha, por exemplo - você usará o comando
para revogar sua chave.

O comando

::

    gpg --output revoke.asc --gen-revoke ID

revogará minha chave cuja identificação é ID (que pode ser tanto o nome
do par de chaves, como o seu número ou o email correspondente) e gerará
um certificado de revogamento no arquivo ``revoke.asc``. Esse
certificado serve para ser enviado a quem tiver minha chave pública para
que saibam que cancelei meu par de chaves. É uma espécie de assinatura
de cancelamento.

Outros comandos
---------------

Para maiores informações sobre como usar o gpg em modo texto, consulte
as `Referências <referencias>`_ ou então digite no seu terminal

::

    man gpg

Resumão: tabela de consulta rápida
----------------------------------

O GPG no modo texto apresenta muitos comandos e frequentemente nos
esquecemos dos parâmetros e da ordem pela qual eles precisam ser
passados ao programa.

- Criar par de chaves: ``gpg --gen-key``
- Compartilhar chave pública: ``gpg --export --armor -o chave.asc email@do.usuario``
- Enviar chaves a um servidor: ``gpg --keyserver servidor.de.chaves --send-keys nome-da-chave``
- Listar chaves do seu chaveiro: ``gpg --list-keys``
- Importar chaves: ``gpg --import nome-do-arquivo``
- Procurar chave num servidor: ``gpg --search-keys email-ou-nome``
- Receber chaves de um servidor: ``gpg --recv-keys id-da-chave``
- Assinar em texto simples: ``gpg --clearsign nome-de-arquivo (opcional)``
- Verificar assinatura: ``gpg --verify nome-do-arquivo``
- Criptografar mensagem: ``gpg -e -a -r nome-ou-mail``
- Criptografar em arquivo: ``gpg -r nome-ou-email -e -a nome-do-arquivo``
- Descriptografar: ``gpg -d nome-do-arquivo``
- Ver impressão digital: ``gpg --fingerprint nome-ou-email``
- Atualizar chaves públicas de um servidor: ``gpg --refresh-keys``
