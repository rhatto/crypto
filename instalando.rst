Instalando programas de criptografia
====================================

Neste tutorial ensinamos apenas como utilizar o programa `GNU Privacy
Guard <http://www.gnupg.org>`_, que é uma ferramenta de criptografia
inteiramente baseada em software livre. Aqui trataremos o uso do GPG no
modo texto e no modo gráfico. Leia nota sobre modo texto e modo gráfico.

Instalando o GPG
----------------

Existem duas opções para usar o GPG: modo texto, onde você digita os
comandos manualmente, e o modo gráfico, mais intuitivo e recomendado
para quem ainda não tem familiaridade com criptografia.

Todos os programas para o Modo Gráfico são na verdade extensões do GPG
em modo texto, o que significa que todos os programas de criptografia
aqui listados sempre vão utilizar o mesmo chaveiro, ou seja, tanto
seu(s) gerenciador(es) de chaveiro como seu(s) programa(s) de email
utilizarão sempre as mesmas informações de chave pública e privada.

Nas seções seguintes, damos instruções de como instalar o GPG no modo
texto, tanto no Windows quanto no Linux ou MacOSX. Se preferir, você
pode pular a parte Modo Texto e ir direto para a seção Modo Gráfico.

Modo Texto no GNU/Linux
-----------------------

Se você usa GNU/Linux ou outro ``*NIX`` (BSD like, etc), provavelmente
sua distribuição deve ter um pacote do GPG. A instalação do mesmo
depende de qual distribuição de linux você usa. Aqui ensinaremos como
instalar nas distribuições mais populares.

Se você usa o Indymix, todos os programas de criptografia que você
necessita já estão instalados e você pode pular para a próxima seção!

Se o seu Linux é o Debian ou compatível (como Kurumin, Knoppix ou
Gnoppix), entre na internet, abra um terminal e digite o comando para
instalar o GPG:

::

    su -c "apt-get update ; apt-get install gnupg"

Se você usa outra distribuição de Linux, procure na documentação
específica do seu sistema como fazer isso, ou baixe os fontes do
programa e compile. Se você usa MacOS, tente o MacGPG.

Modo Gráfico no GNU/Linux
-------------------------

No Linux, nós recomendamos que você use o GPA: GNU Privacy Assistant.
Sua instalação depende de qual distribuição de linux você usa. Aqui
ensinaremos como instalar nas distribuições mais populares. Se você usa
Indymix, todos os programas de criptografia que você necessita já vem
instalados e você pode pular para a próxima seção!

Se você usa Debian ou compatível (como Kurumin, Knoppix ou Gnoppix),
basta se conectar à internet, abrir um terminal e dar o comando para sua
instalação:

::

    sudo apt-get update ; sudo apt-get install gpa

Em seguida, digite a senha de administrador do sistema, caso esta seja
pedida.

Para outras distribuições, consulte a documentação correspondente. Ou,
se você preferir, baixe o código fonte do GPA e compile-o você mesmo.
