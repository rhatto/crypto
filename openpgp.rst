Criptografia com OpenPGP
========================

A criptografia é o método de codificar mensagens de modo a garantir que
só a pessoa que tenha em mãos o código correto possa lê-la.

Para que serve a criptografia? Em resumo, ela serve para

-  Proteger informações
-  Preservar a privacidade das pessoas
-  Permitir a autenticidade de informações (assinatura digital)

Proteger informações significa que você pode escolher quem acessa suas
informações. Preservar a privacidade das pessoas quer dizer que quem não
estiver autorizado a acessar suas informações não conseguirá
decifrá-las. Permitir a autenticidade de informações evita que falem em
seu nome coisas que você não disse.

Como funciona
-------------

A criptografia que veremos aqui é baseada num princípio de dificuldade
de realizar operações reversíveis. Por exemplo, é muito fácil saber qual
é o produto de dois números primos - números que só são divisíveis por 1
e por eles mesmos, como 7, 11, 23. Mas é realmente muito dificil saber,
dado um número qualquer, quais são os números primos que eu preciso
multiplicar para obter esse número. Ou seja, essas operações matemáticas
com números primos são reversíveis - se eu sei quais são os números
primos eu sei o produto deles e se eu sei qual é o número eu posso
descobrir quais são seus fatores primos e são mais difíceis de se
realizar de um sentido do que de outro.

A dificuldade de descobrir quais são os fatores primos de um número
aumenta exponencialmente com o tamanho do número. Exponencialmente quer
dizer realmente muito. É nisso que a criptografia se apóia.

Imagine um cadeado com duas chaves. Uma somente fecha o cadeado e a
outra somente abre. É mais ou menos assim que o GPG funciona. Você
criará duas chaves, uma pública e outra privada. A chave pública é a que
"fecha" (criptografa) e a chave privada é a que "abre" (decifra) depois
que você fornece sua frase-senha.

As pessoas que querem enviar documentos critografados para você devem
ter a sua chave pública e SOMENTE VOCÊ deve ter sua chave privada e deve
saber sua frase-senha. De modo análogo, se você quiser mandar uma
mensagem codificada para uma pessoa, você precisa ter a cave pública da
mesma.

O esquema de criptografia baseado em par de chaves funciona assim:

::

    mensagem original -> chave pública -> mensagem codificada
    mensagem codificada -> chave privada -> mensagem original

Essas operações são reversíveis, ou seja, é possível termos, por exemplo

::

    mensagem codificada -> chave pública -> mensagem original 

o que, em outras palavras, significa a "quebra" da mensagem codificada e
consequentemente a invasão da sua privacidade, pois alguém pode
interceptar a mensagem codificada e junto com sua chave pública
determinar qual é a mensagem original. Calma, não se assuste! Apesar
disso ser perfeitamente possível, devemos lembrar que essas operações
são reversíveis mas também são mais fáceis de fazer num sentido do que
em outro, isto é, é muito mais fácil fazer:

::

    mensagem original -> chave pública -> mensagem codificada

do que

::

    mensagem codificada -> chave pública -> mensagem original

Pra você ter uma idéia, a primeira operação demora tipicamente alguns
segundos, dependendo do tamanho da mensagem e do tamanho da chave. Já a
segunda demora tipicamente milhares de anos (literalmente) e precisa de
um poder computacional enorme, de milhares de computadores ou
supercomputadores trabalhando juntos. Essa dificuldade de fazer a
operação reversa é que possibilita o uso da criptografia como ferramenta
de privacidade pessoal, pois na prática é inviável quebrar mensagens
criptografadas.

A operação

::

    mensagem codificada -> chave privada -> mensagem original

também demora apenas alguns segundos para ser realizada.

Resumindo: a criptografia tratada neste pequeno manual é baseada em
pares de chaves, uma chave pública e uma chave privada. Você troca sua
chave pública com outras pessoas e mantém sua chave privada em segredo.
Se você quise mandar uma mensagem codificada para alguém, é só usar a
chave pública dessa pessoa para criar uma mensagem que só ela poderá
ler. De mesma forma, se alguém quiser lhe enviar uma mensagem
codificada, basta que essa pessoa tenha a sua chave pública e só você
será capaz de ler a mensagem, e para isso você deverá usar sua chave
privada. É muito importante que você tenha esse princípio de
funcionamento bem claro em sua mente antes de continuar.

A coleção de sua(s) chave(s) pública(s) e privada(s) e mais as chaves
públicas de outras pessoas que você possui é denominada de chaveiro.

Exemplo de chave pública
------------------------

Uma chave pública é uma sequência de códigos, e pode ser apresentada
como um arquivo de texto comum. Esse arquivo conterá um monte de
caracteres malucos. Veja só:

::

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: GnuPG v1.0.6 (GNU/Linux)
    Comment: For info see http://www.gnupg.org

    mQGiBD6cuEoRBACQ6QKhCjN2qKHmOeeOdW9wOnnnd9V0eLREreSmMsRD6kSdXnrG
    LQGrXMCBjelwOKB/vh2mcKn646PmSaq4sC+bLSOMAhME/IaDyDWZNWXo37yYlhDu
    VD5wZGZNGMwov/bPDvjNjJTSXlo4/glbUNyVU2n9kiFYoDoSGjg1jQcYLwCgyeOJ
    tHFSoelA62s+YmypI3KAzkcD/1S7Fim4p5X6HA/mUkFtuExDggba+OKmsmYNfPZ/
    Q0sPAK85Or0zKAbGXAKUqezCuKCZ6FaesbIU5hTcQb2zKmzg1hzmtsWCTrxuj2X8
    c//yRjDzFjT8KKCYKZWBGQTapGhvlHHq8ZsLlYZRzYyYtZ13a809FWk8G0Aq5/FA
    be3KA/4nK+XAZUwXqXzu+xKINMu98zi9QWnVfBA5G132uCyrwOTBG1BV3803QFxG
    XdBrEnGlz/RU9xjkVnEBWyqriO+lGDXuEZ6pWyx70UJ1S2qPDYKxoLTB68ZWAfKU
    xYP8JYBC06hk6Ztq7FjkQGHxKMYQ9HCC3l9octyNsux+b/5Q+rQhU2lsdmlvIFJo
    YXR0byA8cmhhdHRvQHJpc2V1cC5uZXQ+iFcEExECABcFAj6cuEoFCwcKAwQDFQMC
    AxYCAQIXgAAKCRDA5viFRw5zwno5AKCGTBDCw9dalpr+SG7MQx00u1iehwCeKuGC
    1FljdhHcu2CQX/JZoWLB/Qi5AQ0EPpy4TRAEAKmsLjxI2k/ITXu2kZJ7+SQPftb9
    yRs8FxOsnmytkUwO8HaryPfuoMU51xG8XYL4blGL6u2J67KJOO4R3buwUDmH16RC
    +nmMNlnaa0zlozsYIuB+r3s18hNLAss1LX0P0Ob6Ownar5VM8yNgVhEZkwBs6VhV
    lfinYThWmpaXXLU3AAMFA/498Mfl1rs4z6vzkmIGu3Mqy+2CXSA/oCp9zPfFLJNM
    +WUGhpbxkbbsNEzHdTFWPcoHi23k01KjT5CAiyiP30o6g8OV+3/WRYqeR4UNT6e8
    7JDZo8kzjnTigI7XoAkqTJhL8pzhzvjboGZAaN1LDPgO2H//iRaBUjjrGaOTl9x2
    c4hGBBgRAgAGBQI+nLhNAAoJEMDm+IVHDnPCsrYAoL9sLObVCteWmkAPFL3b5e/p
    UFfAAKCzRRY3tPu6sHczFzOcw3SzeDN5x5kBogQ+nLhKEQQAkOkCoQozdqih5jnn
    jnVvcDp553fVdHi0RK3kpjLEQ+pEnV56xi0Bq1zAgY3pcDigf74dpnCp+uOj5kmq
    uLAvmy0jjAITBPyGg8g1mTVl6N+8mJYQ7lQ+cGRmTRjMKL/2zw74zYyU0l5aOP4J
    W1DclVNp/ZIhWKA6Eho4NY0HGC8AoMnjibRxUqHpQOtrPmJsqSNygM5HA/9UuxYp
    uKeV+hwP5lJBbbhMQ4IG2vjiprJmDXz2f0NLDwCvOTq9MygGxlwClKnswrigmehW
    nrGyFOYU3EG9syps4NYc5rbFgk68bo9l/HP/8kYw8xY0/CigmCmVgRkE2qRob5Rx
    6vGbC5WGUc2MmLWdd2vNPRVpPBtAKufxQG3tygP+JyvlwGVMF6l87vsSiDTLvfM4
    vUFp1XwQORtd9rgsq8DkwRtQVd/NN0BcRl3QaxJxpc/0VPcY5FZxAVsqq4jvpRg1
    7hGeqVsse9FCdUtqjw2CsaC0wevGVgHylMWD/CWAQtOoZOmbauxY5EBh8SjGEPRw
    gt5faHLcjbLsfm/+UPq0IVNpbHZpbyBSaGF0dG8gPHJoYXR0b0ByaXNldXAubmV0
    PohXBBMRAgAXBQI+nLhKBQsHCgMEAxUDAgMWAgECF4AACgkQwOb4hUcOc8J6OQCg
    hkwQwsPXWpaa/khuzEMdNLtYnocAnirhgtRZY3YR3LtgkF/yWaFiwf0IuQENBD6c
    uE0QBACprC48SNpPyE17tpGSe/kkD37W/ckbPBcTrJ5srZFMDvB2q8j37qDFOdcR
    vF2C+G5Ri+rtieuyiTjuEd27sFA5h9ekQvp5jDZZ2mtM5aM7GCLgfq97NfITSwLL
    NS19D9Dm+jsJ2q+VTPMjYFYRGZMAbOlYVZX4p2E4VpqWl1y1NwADBQP+PfDH5da7
    OM+r85JiBrtzKsvtgl0gP6Aqfcz3xSyTTPllBoaW8ZG27DRMx3UxVj3KB4tt5NNS
    o0+QgIsoj99KOoPDlft/1kWKnkeFDU+nvOyQ2aPJM4504oCO16AJKkyYS/Kc4c74
    26BmQGjdSwz4Dth//4kWgVI46xmjk5fcdnOIRgQYEQIABgUCPpy4TQAKCRDA5viF
    Rw5zwrK2AKC/bCzm1QrXlppADxS92+Xv6VBXwACgs0UWN7T7urB3MxcznMN0s3gz
    ecc=
    =1J0v
    -----END PGP PUBLIC KEY BLOCK-----

Esta é a minha chave pública. Ela é grande e bonita, mas não muito
compreensiva. Para falar a verdade, eu não entendo nada do que está
escrito lá, com a exceção dos seguintes pedaços:

::

    -----BEGIN PGP PUBLIC KEY BLOCK-----

Esse primeiro texto vai informar ao leitor que este é o começo da chave
pública.

::

    Version: GnuPG v1.0.6 (GNU/Linux)
    Comment: For info see http://www.gnupg.org

Essas duas linhas informam sobre qual programa que criou a chave pública
e em qual sistema operacional.

::

    -----END PGP PUBLIC KEY BLOCK-----

Essa linha avisa ao leitor que a sequência de caracteres malucos da
chave pública acabou.

Quando você vai mandar uma mensagem criptografada pra alguém, o programa
de criptografia vai usar os caracteres malucos que ficam entre os textos
``-----BEGIN PGP PUBLIC KEY BLOCK-----`` e
``-----END PGP PUBLIC KEY BLOCK-----`` da chave pública da pessoa pra
criar a mensagem codificada.

OBS: Não há nenhuma restrição quanto ao nome de arquivo que uma chave
pública pode ter. Usualmente é um nome de arquivo com extensão .asc,
.key ou qualquer outra coisa. Por exemplo, o arquivo de chave pública do
seu amigo Groucho pode ser groucho.asc, groucho.key ou qualquer outra
coisa. Se você quiser conferir se um arquivo contém uma chave pública,
basta abri-lo num editor de textos qualquer e ver se o conteúdo do
arquivo se parece com o exemplo de chave pública acima.

Exemplo de chave privada
------------------------

Uma chave privada tambem é uma sequência de códigos que também pode ser
apresentada como um arquivo de texto comum. Esse arquivo conterá um
monte de caracteres malucos. Aqui segue um exemplo fictício de chave
privada:

::

    -----BEGIN PGP PRIVATE KEY BLOCK-----
    Version: GnuPG v1.2.1 (GNU/Linux)

    mQGiBD6cuEoRBACQ6QKhCjN2qKHmOeeOdW9wOnnnd9V0eLREreSmMsRD6kSdXnrG
    LQGrXMCBjelwOKB/vh2mcKn646PmSaq4sC+bLSOMAhME/IaDyDWZNWXo37yYlhDu
    VD5wZGZNGMwov/bPDvjNjJTSXlo4/glbUNyVU2n9kiFYoDoSGjg1jQcYLwCgyeOJ
    tHFSoelA62s+YmypI3KAzkcD/1S7Fim4p5X6HA/mUkFtuExDggba+OKmsmYNfPZ/
    Q0sPAK85Or0zKAbGXAKUqezCuKCZ6FaesbIU5hTcQb2zKmzg1hzmtsWCTrxuj2X8
    c//yRjDzFjT8KKCYKZWBGQTapGhvlHHq8ZsLlYZRzYyYtZ13a809FWk8G0Aq5/FA
    be3KA/4nK+XAZUwXqXzu+xKINMu98zi9QWnVfBA5G132uCyrwOTBG1BV3803QFxG
    XdBrEnGlz/RU9xjkVnEBWyqriO+lGDXuEZ6pWyx70UJ1S2qPDYKxoLTB68ZWAfKU
    xYP8JYBC06hk6Ztq7FjkQGHxKMYQ9HCC3l9octyNsux+b/5Q+rQhU2lsdmlvIFJo
    YXR0byA8cmhhdHRvQHJpc2V1cC5uZXQ+iFcEExECABcFAj6cuEoFCwcKAwQDFQMC
    vUFp1XwQORtd9rgsq8DkwRtQVd/NN0BcRl3QaxJxpc/0VPcY5FZxAVsqq4jvpRg1
    7hGeqVsse9FCdUtqjw2CsaC0wevGVgHylMWD/CWAQtOoZOmbauxY5EBh8SjGEPRw
    gt5faHLcjbLsfm/+UPq0IVNpbHZpbyBSaGF0dG8gPHJoYXR0b0ByaXNldXAubmV0
    NS19D9Dm+jsJ2q+VTPMjYFYRGZMAbOlYVZX4p2E4VpqWl1y1NwADBQP+PfDH5da7
    OM+r85JiBrtzKsvtgl0gP6Aqfcz3xSyTTPllBoaW8ZG27DRMx3UxVj3KB4tt5NNS
    PohXBBMRAgAXBQI+nLhKBQsHCgMEAxUDAgMWAgECF4AACgkQwOb4hUcOc8J6OQCg
    hkwQwsPXWpaa/khuzEMdNLtYnocAnirhgtRZY3YR3LtgkF/yWaFiwf0IuQENBD6c
    uE0QBACprC48SNpPyE17tpGSe/kkD37W/ckbPBcTrJ5srZFMDvB2q8j37qDFOdcR
    vF2C+G5Ri+rtieuyiTjuEd27sFA5h9ekQvp5jDZZ2mtM5aM7GCLgfq97NfITSwLL
    7JDZo8kzjnTigI7XoAkqTJhL8pzhzvjboGZAaN1LDPgO2H//iRaBUjjrGaOTl9x2
    c4hGBBgRAgAGBQI+nLhNAAoJEMDm+IVHDnPCsrYAoL9sLObVCteWmkAPFL3b5e/p
    1FljdhHcu2CQX/JZoWLB/Qi5AQ0EPpy4TRAEAKmsLjxI2k/ITXu2kZJ7+SQPftb9
    ecc=
    =1J0v
    -----END PGP PRIVATE KEY BLOCK-----

Como você deve ter percebido, uma chave privada é uma porção de texto
cuja estrutura é igual à da chave pública. As primeiras duas linhas,

::

    -----BEGIN PGP PRIVATE KEY BLOCK-----
    Version: GnuPG v1.2.1 (GNU/Linux)

informam ao programa de criptografia que o texto que vem na sequência é
uma chave privada. O programa assumirá que o resto do texto é uma chabe
privada até encontrar a linha

::

    -----END PGP PRIVATE KEY BLOCK-----

Exemplo de mensagem criptografada

Como exemplo, que tal criptografarmos a seguinte mensagem:

::

    Seu pai é careca?

O resultado, quando criptografado com minha chave pública, é

::

    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    hQEOA3v8xQeh8DSxEAP/Rks1pm5WOyNZTtlJP1FVK602ix872ZMwsOsOYZ9oBC+o
    pN9/03jLjlk3hF0v9KJkq3SQxu+E7zSCbuMigAum0QZgV3BMVjZDGKLDUN3D8Sgt
    fBju6Y7Vn6wK87OWQlayWMAab+t77wQPjKuH9IPYJwsZ6zJJK5YIhgIlKU0FoKAD
    /RWDmqQua/iswijTh5StgO+FJuseec22TgGPzZC4D05s9JE7gMIO3GvSTGR+re4i
    SaCzrppudxOacsuCcRhR6IkA6lT8fKaZImU/aMev/UgwadGP3XeqiwxPUt+qHg6H
    iYZyMShKAMZzF+PdgflaDYGffgIQBXpxWxjRMufX9LTW0lYBo1mQ6W1XgJG3AY47
    knPyqSKRfuNPwayAGRdZM2yoq/DnwIUGB5cyfiNqkV9lXl25uXud99T3mDojYLzr
    N/pWTHTdu5UA/RbBrPaeZ7HX3tdXnhlCzw==
    =Jqqw
    -----END PGP MESSAGE-----

E aí, você acha que dá pra decifrar? Só com minha chave privada!

As primeiras duas linhas,

::

    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

indicam que o que vem na sequência é uma mensagem criptografada. O fim
da mensagem criptografada é assinalada na linha

::

    -----END PGP MESSAGE-----

Agora vou codificar a mesma mensagem utilizando várias chaves públicas,
para que mais pessoas possam lê-la.

::

    mensagem original -> várias chaves públicas -> mensagem cifrada para vários destinatários

O resultado é

::

    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    hQEOAwXhZ7S+8an6EAP8CwSfq+TjKziYiM4QGwAwNynKDLSkMdw6KeTmSi3XgfP+
    Wf6p6KWGuVmD7ffGMwCBuB+mKpjmXhZ9EyR4JAVpgkMMN2ZCrTcMu4tDZqqvJV/t
    jws1dwg0QaeCdmA6jbxBDuWUsbgOzxY/5URmttCfKGTgIOOh73VDZ3HXM4MrWIcE
    AKFaCgiMybhYZLlXqEpqgyJWH54fsXSgRbv2yZE2G+9aNEolAVji7NfluKGhrosF
    gRCEuFc1pGRnF+Z5+aMj/xK/9DKeqGt97aQbViQ33s/3soqskBWzd95rKK1ZyBRn
    iIEoEXlc+eDRk+j6EsjR+Ex87fU4gCM9aNPQGkdLZZpJhQEOAzrq0korX+quEAQA
    gOEV5STP2zCgQ5sSljd6D02lLXVky5P/4vvgBPUUpH9svMQUF5iJTPc8H/YUoOmW
    Hpmk6nwXDsAwDhyoTuiiPHRBd+D1ckCtwsjRZePOhkU2r0m+9k4kI3Q34XX3jPDI
    YABHRWP0tJaazCU/1PD1rKA0JWoLmA3x3i2AEizwLrkD/R+8xBgOUEV+XNJSEXqS
    ooHbKCu9dItIXjHj2kzTpA5F9anT0xZYG+Q9SivWavZVAd8KLcDM7My1s/udjGyp
    KWeSxcyHu6rYmdJw1by+eWddKn+2W7iSgnaOhjEimZcy53wYmrh5Wzv5vDL0Qd2d
    RSX12GjdqW9FmOQvag2q8sIAhQIOA7jcJ6nlDzObEAf9EPB7HaCBFSovTp/c2GsU
    HEyc3gRQ2RD1XPlnKA/PhE5zTIGCXiRdVLfQuoZ4xOwHng8ppM5xJkIU5oohhysu
    m7YFlkM7rG9kavVAoATDeNfoLXjA7q/Cntl9xyy2NEUU9oQCilrS6JTvJ9UPPY1U
    kW9zS8CNNIAgIQ+rVJZtc1E+34N8xORk+4nQz4QtY0nE6XMN83bIYfoIxGc0dn8O
    PVOz5ej6utcZV2sdTZc0JnX3lXonUhkSCvsDcxRfUOemfObR2XZMq7OrFoKQ1mly
    TQ9AW2mUyEbEtVsFdT8HOAC3M7z/EmnK97nAykONYko1RrHgWY/ekhElqxfvzy2I
    6Qf9F9tTLshUy7QdM7WiGoJaxPwTUwlzGw29rnZo36JNKyWW8qnGU5+YH5iKzM4u
    xeAOt12SKcBgzN2ucEdmOxm0A1rYL0IgDLWTYnD/YkmJbSp4xhwKEUEF4juiJdUd
    ZTf1+tF6Q6yIRIFwLy3ES3NX6sA/dxkiN2YROC41VtcGqn84lCj3c4VnMw7LcLtX
    GBP9y76tObNEXsCYGIsVqjxMXDFh0smQ8v1Loo1qFxBCjuPXWWBWL77814o2rS+Y
    1eH+Q9rv5RajMygDD1afYxKoI1mey7yuRbiG02owHDNMs6wECCH77bRnxAVRLSWD
    UCsIRMk0qWKApD/M7qIWn4qtl9JWAcG4KKrOGdIYswARUoJIMKn+TMJmbA8TxTKo
    J7yrXniv3C9pyeEPqRUWdZqo4szVDKPGLz+VMCV4tzSE5iINGBNNXAM9HxqwmAX9
    tqLVyGhs9UVe8RY=
    =cbe0
    -----END PGP MESSAGE-----

Percebeu como mensagem criptografada ficou maior? É que agora ela possui
uma codificação para cada chave pública.

Fingerprint (Impressão Digital) 
--------------------------------

Tudo bem, alguém me manda sua chave pública, eu a adiciono no meu
chaveiro e começo a utilizá-la para encriptar coisas. Mas quem me
garante de que a chave que recebi realmente é daquela pessoa?

Chegamos a um dos pontos mais delicados da criptografia usando par de
chaves: o que garante que a chave pertence àquela pessoa? Isso será
melhor discutido na seção `O limite da confiabilidade <#limite>`_.

Existe uma maneira de confirmar a procedência da chave utilizando a
impressão digital dessa chave pública, que nada mais é do que um número
associado àquela chave. A idéia é que você possa confirmar a impressão
digital da chave pública com a pessoa proprietária, de preferência ao
vivo ou de alguma forma bem segura. É uma das melhores maneiras de se
certificar que a chave realmente pertence à pessoa.

Por exemplo, alguém me envia uma chave pública. Eu a adiciono ao meu
chaveiro. Quando tiver a oportunidade de encontrar a pessoa ao vivo,
troco com ela nossas impressões digitais, isto é, passo pra ela a
impressão digital da minha chave pública - que mantenho anotada num
papelzinho dentro da minha carteira! - e ela me passa sua impressão, que
também estava anotada num papelzinho! Quando chegar em casa, posso
confirmar se a chave pública dela que tenho no meu computador tem a
mesma impressão digital que me foi passada durante o encontro. Se as
duas coincidirem, posso ter quase certeza de que aquela chave realmente
pertence à pessoa em questão. Eu digo quase pois nós que usamos
criptografia somos muito céticos e paranóicos.

Aqui segue um exemplo de impressão digital de uma chave pública:

::

    268F 448F CCD7 AF34 183E  52D8 9BDE 1A08 9E98 BC16

Não é nada complicado manter esse conjunto de letras num papelzinho ou
impresso no seu cartão de visitas! A experiência mostra que é mais
eficaz imprimir do que copiar manualmente, uma vez que é muito errar na
escrita dos caracteres. Mas se você imprimir, confira o fingerprint
impresso para evitar uma impressora mal intencionada :P

Assinaturas digitais
--------------------

Na seção anterior explicamos como a criptografia pode ser utilizada para
criptografar e descriptografar informações. O uso da criptografia não se
restringe a isso. Com ela, é possível criar assinaturas digitais.

Quando você fecha um contrato, você assina à caneta na linha pontilhada.
Espera-se que você tenha lido tudo direitinho e corcorde com o que
estava escrito. Em outras palavras, você dá confiança naquele documento
e assina em baixo. Sua assinatura é a prova de que você deu sinal verde.
Quanto mais complicada, cheia de garranchos e rabiscada for sua
assinatura, melhor para você, pois dificilmente alguém conseguirá
falsificar sua assinatura.

Com a criptografia baseada em chaveiro a coisa é mais ou menos assim.
Bem mais ou menos. É parecida no sentido de que você precisa confiar no
que está assinando. Mas diferente no sentido que ninguém conseguirá
falsificá-la (a não ser que roubem sua chave privada e sua senha).

A assinatura funciona da seguinte maneira: eu escrevo um texto, por
exemplo:

::

    Torta na cara, torta no pé, torta onde quiser!

Em seguida, utilizo a seguinte operação:

::

    mensagem original -> chave privada -> mensagem assinada

A mensagem assinada conterá a informação da mensagem original e virá
acompanhada de uma porção de texto gerada pela combinação da mensagem
original, da minha chave privada e muitas operações matemáticas malucas.
Essa porção de texto é a assinatura digital. Se alguém receber essa
mensagem e possuir minha chave pública, poderá testar se essa mensagem
tem uma assinatura correta de minha chave privada:

::

    mensagem assinada -> chave pública -> confirmação da assinatura

Se eu assinar a mensagem anterior, a mensagem assinada será

::

    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA1

    Torta na cara, torta no pé, torta onde quiser!
    -----BEGIN PGP SIGNATURE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    iD8DBQFAuSSqvSy1nGtWZ3cRAvbuAJ9CZgA3YXCWCWiHMgtqA1pnjUqnaQCgvvrv
    JRwCqz/lUTdhE0j5KzoMvX0=
    =l6xH
    -----END PGP SIGNATURE-----

De modo análogo ao caso da mensagem criptografada, as linhas

::

    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA1

indicam que o que vem a seguir está assinado digitalmente. Os caracteres
malucos são a assinatura em si. Um programa que tenha essa assinatura e
a chave pública correspondente à chave privada que a assinou pode
verificar se a assinatura confere.

Por fim, a linha

::

    -----END PGP SIGNATURE-----

avisa o programa de criptografia que encerrou a porção de texto que
contém a assinatura.

Eu também poderia enviar minha mensagem num arquivo e a assinatura
separadamente, para facilitar a leitura da mensagem. Por exemplo, a
assinatura correspondente à mensagem

::

    Mensagem protegida contra estelionato.

Pode ser distribuída num arquivo em separado, cujo conteúdo, se assinado
com minha chave pública, será:

::

    -----BEGIN PGP SIGNATURE-----
    Version: GnuPG v1.2.3 (GNU/Linux)

    iD8DBQBALBiEvSy1nGtWZ3cRAmneAKCzNFcwvmIYUS4/t9x9jeAWTCLcygCfSbpT
    UrUnpqD+GUfovjNiS56dCec=
    =8Czp
    -----END PGP SIGNATURE-----

A assinatura digital permite até que você assine sua cópia da chave
pública dos seus amigos e amigas. Uma vez que você trocou sua impressão
digital com a deles, assinar a sua cópia de uma chave pública de outra
pessoa indica que você confia naquela chave pública.

O limite da confiabilidade 
---------------------------

Como foi dito na seção `Fingerprint (Impressão
Digital) <#fingerprint>`_, não há nada que garanta a origem das chaves
públicas que recebemos e adicionamos ao nosso chaveiro.

O que podemos fazer é, além de recebermos ao vivo as impressões digitais
das chaves públicas, dar níveis de confiança a determinadas chaves. Dar
um nível de confiança às vezes é a única coisa que podemos fazer quando
não tivermos a oportunidade de trocas as impressões digitais.

Algo muito popular é o uso da rede de confiabilidade, que é mais ou
menos baseada no princípio do amigo de amigo meu é meu amigo. Por
exemplo, Maria tem a chave pública de João e ambos já trocaram suas
impressões digitais e assinaram as chaves públicas um do outro. Maria
ainda tem a chave pública de Josefina mas nunca poderão trocar suas
impressões digitais ao vivo, pois Josefina mora na Cracóvia e lá é muito
longe. João, porém, já foi até a Cracóvia, teve a oportunidade de trocar
sua impressão digital com Josefina e assinou a chave pública dela. Com
isso, Maria poderá ter confiança na chave de Josefina, pois como Maria
confia na chave de João e João confia na chave de Josefina, Maria pode
também confiar na chave de Josefina, que poderá confiar na chave de
Maria.

Criando essa teia de confiabilidade, as pessoas podem ter um pouco mais
de segurança no uso da criptografia. O importante é confiar não só que
as chaves públicas que você possui realmente pertencem aos seus amigos,
mas também confiar nos seus amigos, pois eles assinarão chaves que
eventualmente você poderá confiar.

Repositórios de chaves
----------------------

Existem alguns computadores na internet que atuam como servidores
públicos de chaves públicas: eles armazenam chaves públicas, podendo
receber e enviar chaves públicas, o que torna mais fácil a busca por
chaves, principalmente de pessoas com as quais você ainda não teve
contato. De uma maneira semelhante ao uso dos sites de busca, você vai
até o servidor de chaves e procura pelo nome da pessoa, seu email, etc,
e a partir disso pode baixar e adicionar chaves ao seu chaveiro.

Histórico
---------

Essa seção é meramente ilustrativa e tem como objetivo dar um tempero à
discussão. Sua leitura pode ser dispensada sem que as próximas seções
fiquem comprometidas.

O cerne do problema, quando falamos em criptografia, é que o meio onde a
mensagem vai se propagar é público, ou seja, qualquer pessoa presente
naquele meio pode capturar a mensagem, seja esse meio uma sala, uma mesa
de bar, o sistema telefônico ou a internet. Desde o começo, essa arte
não teve muito sucesso em criar meios privados para a criculação de
mensagens, o que ela conseguiu foi criar sistemas que impediam que a
mensagem fosse compreendida caso fosse interceptada durante seu trajeto
até o receptor.

O esquema de par de chaves que tratamos aqui, também conhecido como
chaves assimétricas, é talvez a descoberta mais importante no campo da
criptografia nos últimos séculos, mas foi precedida por outras também
muito interessantes e será sucedida por técnicas ainda mais refinadas -
como no caso da criptografia quântica.

Aqui seguem algumas referências muito interessantes sobre a aventura de
cifrar mensagens através dos tempos:

-  `Criptografia na
   Wikipedia <http://pt.wikipedia.org/wiki/Criptografia>`_ (`versão em
   inglês <http://en.wikipedia.org/wiki/Cryptography>`_)
-  `Wikibook sobre
   criptografia <http://en.wikibooks.org/wiki/Cryptography>`_ (inglês)
